package com.book.model;

import javax.persistence.*;

@Entity
@Table(name = "tbl_book")
public class Book {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookID;
	
	private String bookName;
	
	private String bookAuthour;
	
	private String bookPrice;

	public String getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(String bookPrice) {
		this.bookPrice = bookPrice;
	}

	public long getBookID() {
		return bookID;
	}

	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookAuthour() {
		return bookAuthour;
	}

	public void setBookAuthour(String bookAuthour) {
		this.bookAuthour = bookAuthour;
	}

	@Override
	public String toString() {
		return "Book [bookID=" + bookID + ", bookName=" + bookName + ", bookAuthour=" + bookAuthour + "]";
	}
	
	
	
	

}
