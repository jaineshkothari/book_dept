package com.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

	@Autowired
	BookRepository repository;

	@PostMapping("/booksave")
	public String insertBook(@RequestBody Book book) {
		repository.save(book);
		return "Record Save successfully ";
	}
	
	@GetMapping("/ab")
	public String registration()  {
		return "testApi";
	}

}
