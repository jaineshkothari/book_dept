package com.demo.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookDepartmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookDepartmentApplication.class, args);
	}

}
